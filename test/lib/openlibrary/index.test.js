/* eslint-disable import/newline-after-import */
/* eslint-disable global-require */
const axios = require('axios');
const openlibraryApi = require('../../../src/lib/openlibrary');
const mocks = {
  works: require('./works-response.json'),
  authors: require('./authors-response.json'),
  book: require('./book-response.json'),
  author: require('./author-response.json'),
  search: require('./search-response.json'),
};

describe('Openlibrary test', () => {
  beforeEach(() => {
    expect(openlibraryApi).toBeDefined();
  });

  afterEach(() => {
    jest.resetAllMocks();
    jest.restoreAllMocks();
  });

  describe('getBooks method', () => {
    beforeEach(() => {
      expect(openlibraryApi.getBooks).toBeDefined();
    });

    test('should fail if the olid is not valid', async () => {
      await expect(openlibraryApi.getBooks('abc'))
        .rejects
        .toThrow('Invalid OLID');
    });

    test('should work for works', async () => {
      jest
        .spyOn(axios, 'get')
        .mockImplementationOnce(() => Promise.resolve(mocks.works))
        .mockImplementation(() => Promise.resolve(mocks.author));

      const books = await openlibraryApi.getBooks('OL8384934W');
      expect(axios.get).toHaveBeenCalledWith('http://openlibrary.org/query.json?type=/type/edition&works=/works/OL8384934W&*=');
      expect(books.length).toBe(3);

      expect(books[0]).toEqual({
        title: 'The Goal',
        publish: {
          publishers: [
            'Highbridge Audio',
          ],
          date: 'September 7, 2006',
        },
        images: [
          'https://covers.openlibrary.org/b/id/879614-L.jpg',
        ],
        authors: [
          'Eliyahu M. Goldratt',
        ],
      });
    });

    test('should work for authors', async () => {
      jest
        .spyOn(axios, 'get')
        .mockImplementationOnce(() => Promise.resolve(mocks.authors))
        .mockImplementation(() => Promise.resolve(mocks.author));

      const books = await openlibraryApi.getBooks('OL2794070A');
      expect(axios.get).toHaveBeenCalledWith('http://openlibrary.org/query.json?type=/type/edition&authors=/authors/OL2794070A&*=');
      expect(books.length).toBe(20);
      expect(books[0]).toEqual({
        title: 'The Goal',
        publish: {
          publishers: [
            'Gower Pub Co',
          ],
          date: 'August 1996',
        },
        images: [
          'https://covers.openlibrary.org/b/id/375853-L.jpg',
        ],
        authors: [
          'Eliyahu M. Goldratt',
          'Eliyahu M. Goldratt',
        ],
      });
    });

    test('should work for editons', async () => {
      jest
        .spyOn(axios, 'get')
        .mockImplementationOnce(() => Promise.resolve(mocks.book))
        .mockImplementation(() => Promise.resolve(mocks.author));

      const books = await openlibraryApi.getBooks('OL8663885M');

      expect(axios.get).toHaveBeenCalledTimes(2);
      expect(axios.get).toHaveBeenNthCalledWith(1, 'https://openlibrary.org/books/OL8663885M.json');

      expect(axios.get).toHaveBeenNthCalledWith(2,
        'https://openlibrary.org/authors/OL2794070A.json');


      expect(books.length).toBe(1);
      const book = books[0];
      expect(book.title).toBe('The Goal');
      expect(book.subtitle).toBe('A Process of Ongoing Improvement');
      expect(book.images).toEqual([
        'https://covers.openlibrary.org/b/id/802920-L.jpg',
      ]);
      expect(book.authors).toEqual([
        'Eliyahu M. Goldratt',
      ]);
      expect(book.publish).toEqual({
        publishers: ['Highbridge Audio'],
        date: 'November 9, 2000',
      });
    });
  });

  describe('search method', () => {
    test('should work', async () => {
      jest
        .spyOn(axios, 'get')
        .mockImplementation(() => Promise.resolve(mocks.search));

      const books = await openlibraryApi.search('the+goal');

      expect(books[1]).toEqual({
        title: 'The Goal',
        images: ['https://covers.openlibrary.org/b/olid/OL8663885M-L.jpg'],
        link: '/books/OL8384934W',
        subtitle: 'A Process of Ongoing Improvement',
        authors: [
          'Eliyahu M. Goldratt',
        ],
      });
    });
  });
});
