const { Response } = require('jest-express/lib/response');
const controller = require('./../../../src/controllers/books');
const AppError = require('./../../../src/lib/AppError');
const openlibraryApi = require('./../../../src/lib/openlibrary');

let request;
let response;
const OLID = 'OL8384934W';
const bookName = 'the+goal';

describe('controllers book', () => {
  beforeEach(() => {
    response = new Response();
  });

  afterEach(() => {
    response.resetMocked();
  });
  describe('get method', () => {
    beforeEach(() => {
      request = {
        params: {
          olid: 'OL8384934W',
        },
      };
    });

    test('should fail with 400 if AppError', async () => {
      jest.spyOn(openlibraryApi, 'getBooks').mockImplementation(() => Promise.reject(new AppError('A random error')));
      await controller.get(request, response);
      expect(response.status).toBeCalledWith(400);
      expect(response.json).toBeCalledWith({
        message: 'A random error',
      });
    });

    test('should fail if library fails', async () => {
      jest.spyOn(openlibraryApi, 'getBooks').mockImplementation(() => Promise.reject());

      await controller.get(request, response);

      expect(response.status).toBeCalledWith(500);
      expect(response.json).toBeCalledWith({
        message: 'Internal error',
      });
    });

    test('should work', async () => {
      const getResponse = { OLID };
      jest.spyOn(openlibraryApi, 'getBooks').mockImplementation(() => Promise.resolve(getResponse));

      await controller.get(request, response);
      expect(openlibraryApi.getBooks).toHaveBeenCalledWith(OLID);

      expect(response.json).toBeCalledWith(getResponse);
    });
  });

  describe('search method', () => {
    beforeEach(() => {
      request = {
        query: {
          search: bookName,
        },
      };
    });
    test('should fail if no search parameter was sent', () => {
      request.query.search = '';
      controller.search(request, response);
      expect(response.status).toBeCalledWith(400);
      expect(response.json).toBeCalledWith({
        message: "Expecting query parameter named 'search'",
      });
    });

    test('should fail if library fails', async () => {
      jest.spyOn(openlibraryApi, 'search').mockImplementation(() => Promise.reject());

      await controller.search(request, response);

      expect(response.status).toBeCalledWith(500);
      expect(response.json).toBeCalledWith({
        message: 'Internal error',
      });
    });

    test('should work', async () => {
      const searchResponse = { title: 'The goal' };
      jest.spyOn(openlibraryApi, 'search').mockImplementation(() => Promise.resolve(searchResponse));

      await controller.search(request, response);
      expect(openlibraryApi.search).toHaveBeenCalledWith(bookName);

      expect(response.json).toBeCalledWith(searchResponse);
    });
  });
});
