const { Response } = require('jest-express/lib/response');
const controller = require('./../../src/controllers');

let response;

describe('controllers default', () => {
  beforeEach(() => {
    response = new Response();
  });

  afterEach(() => {
    response.resetMocked();
  });

  test('should return correct response if resource not found', () => {
    controller.noResourceFound(null, response);
    expect(response.status).toBeCalledWith(404);
    expect(response.json).toBeCalledWith({
      message: 'Resource not found',
    });
  });
});
