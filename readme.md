Express excercise to connect to Openlibrary RestAPI endpoints and make a wrapper around it.

Objectives:
- Query open library endpoints by OLID or with a title.
- Show json response
- Apply TDD a will.
- List books and display basic like cover image URL title Author OLID...

What is missing:

- 100% test coverage (add library supertests and test src/app.js)
- Pagination.
- Create a Dockerfile.yml
- Move a couple of classes inside src/lib/openlibrary to an adequate place.
- Caching authors on a key-value store to prevent repeated similar queries. (maybe extend it to books as well)

How to run:

- install dependencies:
```
npm install
```
- run
```
node src/app.js
```

- How to use:
Find by title:
```
curl localhost:3000/books?search=the+goal
```
From that results, you can find more details using the link property:
```
curl localhost:3000/books/OL8384934W
```