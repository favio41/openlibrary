const openlibraryApi = require('./../../lib/openlibrary');
const AppError = require('./../../../src/lib/AppError');

module.exports.get = async (req, res) => {
  const { olid } = req.params;

  try {
    const books = await openlibraryApi.getBooks(olid);
    res.json(books);
  } catch (error) {
    if (error instanceof AppError) {
      res.status(400).json({ message: error.message });
      return;
    }
    // console.log('Error when calling OpenlibraryApi.get', { query: req.query, error });
    res.status(500).json({ message: 'Internal error' });
  }
};

module.exports.search = async (req, res) => {
  const search = typeof req.query.search === 'string' ? req.query.search : null;
  if (!search || search === '') {
    res.status(400).json({
      message: "Expecting query parameter named 'search'",
    });
    return;
  }

  try {
    const books = await openlibraryApi.search(search);
    res.json(books);
  } catch (error) {
    // console.log('Error when calling OpenlibraryApi.search', { query: req.query, error });
    res.status(500).json({ message: 'Internal error' });
  }
};
