module.exports.noResourceFound = (req, res) => {
  res.status(404).json({
    message: 'Resource not found',
  });
};
