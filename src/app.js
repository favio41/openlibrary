const express = require('express');
const defaultController = require('./controllers');
const bookController = require('./controllers/books');

const app = express();
const port = 3000;

app.get('/books/:olid', bookController.get);

app.get('/books', bookController.search);

app.all('/*', defaultController.noResourceFound);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

module.exports.app = app;
