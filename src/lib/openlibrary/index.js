const axios = require('axios');
const AppError = require('../AppError');

// TODO: move this to a place that can be reused in the whole application.
axios.interceptors.request.use(
  (request) => {
    console.log('Calling', request.url);
    return request;
  },
  (error) => {
    console.log('Error request', error);
    Promise.reject(error);
  },
);
axios.interceptors.response.use(
  response => response.data,
  (error) => {
    console.log('Error response', error);
    Promise.reject(error);
  },
);

// TODO: move this to a model folder
class OLID {
  static isValid(olid) {
    return !olid || !olid.match(/OL[1-9]\d{0,7}[AMW]/);
  }

  static getType(olid) {
    return olid.split('').pop().toUpperCase();
  }

  static isTypeWork(olid) {
    return this.getType(olid) === 'W';
  }

  static isTypeAuthor(olid) {
    return this.getType(olid) === 'A';
  }

  static isTypeEdition(olid) {
    return this.getType(olid) === 'M';
  }
}

// TODO: move this to a model folder
class Book {
  async loadAuthorsByOlids(olids) {
    // TODO: consider caching authors on a key-value store to prevent repeated similar queries.
    const authors = await axios.all((olids || [])
      .map(author => axios.get(`https://openlibrary.org${author.key}.json`)));

    this.authors = authors.map(author => author.name);

    return this;
  }

  async loadEdition(edition) {
    this.title = (edition.title || '').trim();
    this.subtitle = edition.subtitle;
    this.publish = {
      publishers: edition.publishers,
      date: edition.publish_date,
    };
    this.images = (edition.covers || []).map(cover => `https://covers.openlibrary.org/b/id/${cover}-L.jpg`);

    await this.loadAuthorsByOlids(edition.authors);
    return this;
  }
}

module.exports.getBooks = async (olid) => {
  if (OLID.isValid(olid)) {
    return Promise.reject(new AppError('Invalid OLID'));
  }
  // Holds the editions from the openlibrary API
  const editions = [];

  if (OLID.isTypeWork(olid)) {
    editions.push(...await axios.get(`http://openlibrary.org/query.json?type=/type/edition&works=/works/${olid}&*=`));
  }

  if (OLID.isTypeAuthor(olid)) {
    editions.push(...await axios.get(`http://openlibrary.org/query.json?type=/type/edition&authors=/authors/${olid}&*=`));
  }

  if (OLID.isTypeEdition(olid)) {
    const edition = await axios.get(`https://openlibrary.org/books/${olid}.json`);
    editions.push(edition);
  }

  // Transform the editions into a book object.
  const promises = editions.map((edition) => {
    const book = new Book();
    return book.loadEdition(edition);
  });

  // When everything is ready return.
  return Promise.all(promises);
};

module.exports.search = async (search) => {
  // Find work by title (assumption from the task definition)
  const apiRes = await axios.get(`https://openlibrary.org/search.json?title=${search}`);

  // We don't reuse Book object, because no all properties matches.
  const works = (apiRes.docs || []).map((work) => {
    const result = {
      title: work.title,
      subtitle: work.subtitle,
      authors: work.author_name || [],
      link: `/books/${work.key.split('/').pop()}`,
    };
    result.images = [];
    if (work.cover_edition_key) {
      result.images.push(`https://covers.openlibrary.org/b/olid/${work.cover_edition_key}-L.jpg`);
    } else if (work.cover_i) {
      result.images.push(`https://covers.openlibrary.org/b/id/${work.cover_i}-L.jpg`);
    }
    return result;
  });
  return works;
};
